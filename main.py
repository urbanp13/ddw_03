# coding: utf-8

import nltk
import numpy as np
from networkx.drawing.layout import _rescale_layout, _fruchterman_reingold, _sparse_fruchterman_reingold
from networkx.drawing.nx_agraph import graphviz_layout
from nltk.corpus import brown
from collections import Counter
from string import punctuation
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
from nltk.stem import WordNetLemmatizer
from nltk.sentiment.util import *
from nltk.sentiment import SentimentIntensityAnalyzer
import networkx as nx
from os import path
from wordcloud import WordCloud
import matplotlib.pyplot as plt
from itertools import combinations
import wikipedia
import io

# nltk.download()
# Download Corpora -> brown + webtext + words
# Download Models -> punkt + average_perceptron_tagger + maxent_ne_chunker + vader_lexicon + wordnet + tagsets
def tokenCounts(tokens):
    counts = Counter(tokens)
    sortedCounts = sorted(counts.items(), key=lambda count:count[1], reverse=True)
    return sortedCounts

def extractEntities(ne_chunk):
    data = {}
    for entity in ne_chunk:
        if isinstance(entity, nltk.tree.Tree):
            t = " ".join([word for word, tag in entity.leaves()])
            ent = entity.label()
            data[t] = ent
        else:
            continue
    return data

def filt(x):
    return x.label()=='NVN'


def getWikiInfo(entities):
    f = open('output.txt', 'w')
    for e in entities:
        descNode = []
        entry = ''
        results = wikipedia.search(e)
        #print(results)
        for r in results:
            try:
                entry = r
                sentence = wikipedia.summary(r, sentences=1)
                tokens = nltk.word_tokenize(sentence)
                tagged = nltk.pos_tag(tokens)
                #print tagged
                #grammar ="NP: {<DT>?<JJ>*<NN|NNS>}"
                #ze stredu mozna vyhodit VBN
                #grammar ="NP: {<VBZ|VBD|VBP><JJ|CC|NN|NNS|NNP|NNPS|VBD|VBN|DT|RB|IN|VBG|PRP|.|,|POS>*<NN|NNS|NNPS|NNP>}"
                #gramatika pro pritomny cas
                grammar = '''
                    NP:
                       {<DT>*(<NN.*>|<JJ.*>|<CC>|<VBG>|<VBD>|<,>|<POS>)*<NN.*>}
                    NVN:
                       {<VBZ|VBP><NP>}'''
                #gramatika pro minuly cas
                grammarPast = '''
                    NPSHORT:
                       {<DT>*(<NN.*>|<JJ.*>|<CC>|<VBG>|<,>|<POS>)*<NN.*>}
                    NVN:
                       {<VBD><NPSHORT>}
                    '''

                cp = nltk.RegexpParser(grammar)
                cpPast = nltk.RegexpParser(grammarPast)
                parsed = cp.parse(tagged)
                parsedPast = cpPast.parse(tagged)
                for subtree in parsed.subtrees(filter=lambda x: x.label() == 'NVN'):
                    descNode.append(subtree.leaves())
                    break
                if (descNode.__len__() == 0):
                    for subtree in parsedPast.subtrees(filter=lambda x: x.label() == 'NVN'):
                        descNode.append(subtree.leaves())
                        break
                break
            except:
                continue
        #print '\n\n'
        description = ''
        if (descNode.__len__() == 0):
            description = 'Thing'
        else:
            words = descNode[0]
            i = 0
            for (a,b) in words:
                if i == 0:  #vynechani uvodniho slovesa
                    i = i + 1
                elif i == 1:    #na zacatku neni mezera
                    description = a
                    i = i + 1
                elif a == ',' or a == '.':  #pred interpunkci neni mezera
                    description = description + a
                    i = i + 1
                else:
                    description = description + ' ' + a
                    i = i + 1
        if entry == '':
            entry = u'Not found'
        print ('entity: ' + e + ',\twiki_page_title: ' + entry + ',\textracted_description: ' + description + '\n')
        with io.open('output.txt', 'a', encoding='utf-8') as fg:
            fg.write('entity: ' + e + ',\twiki_page_title: ' + entry + ',\textracted_description: ' + description + '\n')



text = None
with open('doc.txt', 'r') as f:
    text = f.read()
regex = re.compile("[^a-zA-Z .,?!--'\n''\r']")
text = regex.sub('', text)
text = text.replace('\r', ' ').replace('\n', ' ').replace('\n', ' ').replace('--', ' ').replace('...', ' ')

#automaticke rozpoznani entit
#print 'POS tagging:'
#print 'entity recognition:'
tokens = nltk.word_tokenize(text)
tagged = nltk.pos_tag(tokens)
ne_chunked = nltk.ne_chunk(tagged, binary=True)
nltk_entities = extractEntities(ne_chunked)


entity = []     #vyber vlastnich entit
own_entities = []
for tagged_entry in tagged:
    #print tagged_entry
    if (tagged_entry[1].startswith("NN") or (entity and tagged_entry[1].startswith("JJ"))):
        entity.append(tagged_entry)
    else:
        if (entity) and entity[-1][1].startswith("JJ"):
            entity.pop()
        if (entity and " ".join(e[0] for e in entity)[0].isupper()):
            #print (" ".join(e[0] for e in entity))
            own_entities.append(" ".join(e[0] for e in entity))
        entity = []

ownList = list(set(own_entities))  # odstraneni duplikatu

#print(tagged)
#print nltk_entities.__len__()
print nltk_entities
#print own_entities.__len__()
print own_entities
#print ownList.__len__()    #bez duplikatu
print ownList


#sestupne serazene POS a entity
print tokenCounts(tagged)
print tokenCounts(own_entities)

#getWikiInfo(nltk_entities)     #NLTK entity
getWikiInfo(ownList)            #custom pattern entity
